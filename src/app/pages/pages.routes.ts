import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProgressComponent } from './progress/progress.component';
import { Graficas1Component } from './graficas1/graficas1.component';
import { AccoutSettingsComponent } from './accout-settings/accout-settings.component';
import { LoginGuardGuard } from '../services/guards/login-guard.guard';

const pagesRoutes: Routes = [
    {
        path: '', 
        component: PagesComponent,
        canActivate: [LoginGuardGuard],
        children: [
            {path: 'dashboard', component: DashboardComponent, data: { titulo: 'Dashboard'} },
            {path: 'progress', component: ProgressComponent , data: { titulo: 'Progress'}},
            {path: 'graficas1', component: Graficas1Component , data: { titulo: 'Gráficas'} },
            {path: 'account-settings', component: AccoutSettingsComponent , data: { titulo: 'Ajustes del tema'} },
            {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
        ]
     },
];
export const PAGES_ROUTES = RouterModule.forChild( pagesRoutes);