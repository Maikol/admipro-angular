import { Component, Input, Output, OnInit, EventEmitter, ElementRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-incrementador',
  templateUrl: './incrementador.component.html',
  styleUrls: ['./incrementador.component.css']
})
export class IncrementadorComponent implements OnInit {


  @Input() leyenda: string = 'Leyenda';
  @Input() porcentaje: number = 50;

  @Output() cambioValor: EventEmitter<number> = new EventEmitter();

  
  // @ViewChild('txtPorcentaje') txtPorcentaje: ElementRef;

  constructor() { }

  ngOnInit() {

  }

  onChanges( newValue: number) {

    // let elementHTML : any = document.getElementsByName('porcentaje') [0];

    if (newValue >= 100){
        this.porcentaje = 100;
    } else if (newValue <= 0) {
      this.porcentaje = 0;

    } else {
      this.porcentaje = newValue;
    }
  // elementHTML.value = this.porcentaje;
    // this.txtPorcentaje.nativeElement.value = this.porcentaje;
    this.cambioValor.emit(this.porcentaje);
  }
  
  cambiarValor(valor: number) {

    if (this.porcentaje >= 100 && valor > 0) {
      this.porcentaje = 100;
      return;
    }
    if (this.porcentaje <= 0  && valor < 0) {
      this.porcentaje = 0;
      return;
    }

    this.porcentaje = this.porcentaje + valor;

    this.cambioValor.emit(this.porcentaje);
    // this.txtPorcentaje.nativeElement.focus();
  }
}
