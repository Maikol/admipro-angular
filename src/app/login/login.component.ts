import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { UsuarioService } from '../services/usuario/usuario.service';
import { Usuario } from '../models/usuario.model';
import { element } from 'protractor';

declare function init_plugins();
declare const gapi: any;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(public router: Router, public _usuarioService: UsuarioService) { }

  email: string;
  recuerdame: boolean = false;

  auth2: any;
  ngOnInit() {
    init_plugins();
    this.googleInit();
    this.email = localStorage.getItem('email') || '';
    if (this.email.length > 1){
      this.recuerdame = true;
    }
  }

  // iniciar seccion con google
  googleInit(){
    gapi.load('auth2', () => {
        this.auth2 = gapi.auth2.init({
          client_id: '1040047842051-dfq0kfvc0jsa4oo6aq5i4avk6cq35vet.apps.googleusercontent.com',
          cookiepolicy: 'single_host_origin',
          scope: 'profile email'
        });

        this.attachSignin(document.getElementById('btnGoogle'));
    });

  }

  // sacamos el token
  attachSignin( element) {
      this.auth2.attachClickHandler(element, {} ,(googleUser) => {
        // let profile = googleUser.getBasicProfile();

        let token = googleUser.getAuthResponse().id_token;
        this._usuarioService.loginGoole(token)
        .subscribe( () => window.location.href = '#/dashboard');
           
      });
  }

  // Metodo para Iniciar sección 
  ingresar(forma : NgForm) {

    if(forma.invalid){
      return;

    }
    let usuario = new Usuario(null, forma.value.email, forma.value.password);

    this._usuarioService.login( usuario, forma.value.recuerdame)
                .subscribe( correcto => this.router.navigate(['/dashboard'])
        );
    // 
    
  }
}
